class OrdersController < ApplicationController

  before_action :authenticate_user!	

  def index
    @orders = Order.where(:order_delete=>'0')
  end

  def show
    @order  = Order.find(params[:id])
  end

  def new
    @order = Order.new
    @customer = Customer.all
    @staff = Staff.all
  end

  def create
    @order = Order.new(order_params)

    measurement_ids = params[:order][:measurement_ids].select(&:presence)
    if @order.save
      measurement_ids.each do |m_id|
        @order.order_measurements.create({measurement_id: m_id})  
      end
      redirect_to orders_path, notice: 'Order was successfully created.'
    else
      render new_order_path, :alert=> 'Order was not created.'
    end
  end

  def edit
  	@order = Order.find(params[:id])
  	@customer = Customer.all
    @staff = Staff.where(:staff_delete=>'0')
    @measurements = @order.measurements
  end

  def update
    @order = Order.find(params[:id])

  	if @order.update(order_params)
      redirect_to orders_path, notice: 'Order was successfully updated.'
    else
      redirect_to edit_orders_path, :alert=> 'Order was not updated.'
    end
  end

  def destroy
  	if @order = Order.where(:id=>params[:id]).update_all(:order_delete=>'1')
      redirect_to orders_path, notice: 'Order was successfully deleted.'
    end
  end
private
  def order_params
    params.require(:order).permit(:customer_id, :staff_id, :delivery_date, :order_amount, :order_paid, :order_status, :order_details)    
  end
end
