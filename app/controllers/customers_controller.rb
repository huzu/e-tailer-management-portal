class CustomersController < ApplicationController

  before_action :authenticate_user!

  def index
    @customers = Customer.all
  end

  def show
    @customer = Customer.find(params[:id])
    @customer_order = Order.where(:customer_id=> params[:id], :order_delete=>'0')
    @measurements = @customer.measurements

    @customer_order_amount = Order.where(:customer_id=> params[:id], :order_delete=>'0').sum(:order_amount)
    @customer_order_paid = Order.where(:customer_id=> params[:id], :order_delete=>'0').sum(:order_paid)
    @customer_order_total = @customer_order.count
  end

  def new
    @customer = Customer.new
  end

  def create
    begin      
      @customer = current_user.customers.create!(customer_params)      
      if @customer.present?
        redirect_to customer_path(@customer), notice: 'Customer was successfully created.'
      else
        redirect_to new_customer_path, :alert=> 'Customer was not created.'
      end  
    rescue Exception => e
      puts "#{e.message}"
    end
    
  end

  def edit
    @customer = Customer.find(params[:id])
  end

  def update
    @customer = Customer.find(params[:id])

    if @customer.update(customer_params)
      redirect_to customers_path, :notice=> 'Customer was successfully updated.'
    else
      redirect_to edit_customers_path, :alert=> 'Customer was not updated.'
    end
  end

  def destroy
    @customer = Customer.find(params[:id])

    if @customer.destroy
      redirect_to customers_path, :notice=> 'Customer was successfully deleted.'
    end    
  end

  def get_customer_measurements    
    customer = Customer.find(params[:id])
    @customer_measurments = customer.measurements

    respond_to do |format|
      format.json {render :json => @customer_measurments}
    end
  end
private
  def customer_params
    params.require(:customer).permit(:customer_name, :customer_phone, :customer_email, :customer_sex, :customer_address, measurements_attributes: [:name, :neck, :shoulder, :chest, :waist, :hips, :sleeve, :bicep, :wrist, :length, :thigh, :crotch, :ankles, :inseam, :outseam, :other_measurements])
  end
end
