class MeasurementsController < ApplicationController

  before_action :authenticate_user!	

  def new
    @customer = Customer.find(params[:customer_id])
    @measurement = @customer.measurements.build
  end

  def show
    @measurement  = Measurement.find(params[:id])
    @customer = @measurement.customer
  end

  def edit
  	@measurement = Measurement.find(params[:id])
  end

  def create  
    @customer = Customer.find(params[:customer_id])
    @measurement = @customer.measurements.create(measurement_params)

    if @measurement.save
      redirect_to customer_measurement_path(@customer, @measurement), :notice=> 'Measurement was successfully created.'
    else
      redirect_to edit_measurement_path, :alert=> 'Measurement was not created.'
    end
  end

  def update
  	@measurement = Measurement.find(params[:id])

  	if @measurement.update(measurement_params)
      redirect_to customer_measurement_path(@measurement), :notice=> 'Measurement was successfully updated.'
    else
      redirect_to edit_order_path, :alert=> 'Measurement was not updated.'
    end
  end

  def destroy
    @measurement = Measurement.find(params[:id])
    @customer = Customer.find(params[:customer_id])

    if @measurement.destroy
      redirect_to customer_path(@customer.id), :notice=> 'Measurement was successfully deleted.'
    end    
  end
private
  def measurement_params
    params.require(:measurement).permit(:name, :knee, :shoulder, :chest, :waist, :hips, :sleeve, :b_width, :top_length, :bottom_length, :thigh, :p2p, :cuff, :h_front, :bottom)
  end
end
