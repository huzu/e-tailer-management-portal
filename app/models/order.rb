class Order < ApplicationRecord
  belongs_to :customer
  belongs_to :staff
  has_many :order_measurements
  has_many :measurements, through: :order_measurements
end
