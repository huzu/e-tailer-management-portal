class Staff < ApplicationRecord
  has_many :orders
  has_many :salaries
  belongs_to :user
end
