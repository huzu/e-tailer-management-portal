class Measurement < ApplicationRecord
  belongs_to :customer
  has_many :order_measurements
  has_many :orders, through: :order_measurements
end
