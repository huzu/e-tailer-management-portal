class OrderMeasurement < ApplicationRecord
  belongs_to :order
  belongs_to :measurement
end
