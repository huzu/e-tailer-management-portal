class Customer < ApplicationRecord
  has_many :orders
  belongs_to :user
  has_many :measurements
  accepts_nested_attributes_for :measurements
end
