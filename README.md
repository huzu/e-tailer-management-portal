# A Simple Tailor Shop Management System ROSE TMS

## About this application

This application is created by Huzefa. This is a simple tailor shop management system. <br>
##### Framework: Ruby on Rails 5.0.2
##### Language : Ruby 2.2.6

## Install
- Install all gems by typing this:
```
bundle install
```
- To change Database go to "config/database.yml" and then change database. Default Database is SQLite.

- Migrate Database by typing this:
```
rake db:migrate
```
- Sign Up by replacing "sign_in" to "sign_up" from address bar and add all informations.


## Live Demo

##### Live Link: https://enigmatic-tundra-72611.herokuapp.com/
