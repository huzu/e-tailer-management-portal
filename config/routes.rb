Rails.application.routes.draw do

  root :to => 'dashboards#index'

  resources :dashboards
  resources :customers do 
    resources :measurements

    member do 
      get 'get_customer_measurements'
    end
  end
  resources :orders
  resources :staffs
  resources :salaries  

  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end