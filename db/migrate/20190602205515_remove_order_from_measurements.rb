class RemoveOrderFromMeasurements < ActiveRecord::Migration[5.0]
  def change
    remove_reference :measurements, :order
  end
end
