class CreateOrderMeasurements < ActiveRecord::Migration[5.0]
  def change
    create_table :order_measurements do |t|
      t.references :order, foreign_key: true
      t.references :measurement, foreign_key: true

      t.timestamps
    end
  end
end
