class AddCustomerToMeasurement < ActiveRecord::Migration[5.0]
  def change
    add_reference :measurements, :customer, foreign_key: true
  end
end
