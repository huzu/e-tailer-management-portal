class UpdateColumnNamesMeasurements < ActiveRecord::Migration[5.0]
  def change
    rename_column :measurements, :length, :top_length
    rename_column :measurements, :crotch, :bottom_length
    rename_column :measurements, :outseam, :h_front
    rename_column :measurements, :inseam, :cuff
    rename_column :measurements, :ankles, :p2p
    rename_column :measurements, :bicep, :b_width
    rename_column :measurements, :neck, :knee
    rename_column :measurements, :other_measurements, :bottom
  end
end
